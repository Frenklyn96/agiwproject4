import deepmatcher as dm
import pandas as pd
import numpy as np
import torch
import pickle

'''

listing2019= pd.read_csv("Setbandb/listings2019.csv",nrows=10000)
listing2021=pd.read_csv("Setbandb/listings2021.csv",nrows=10000)
data=[]
df = pd.DataFrame(data, columns=['id','left_name','left_host_id','left_host_name',
'left_neighbourhood_group','left_neighbourhood','left_latitude','left_longitude',
'left_room_type','left_price','left_minimum_nights','left_number_of_reviews',
'left_last_review','left_reviews_per_month','left_calculated_host_listings_count',
'left_availability_365','right_name','right_host_id','right_host_name',
'right_neighbourhood_group','right_neighbourhood','right_latitude','right_longitude',
'right_room_type','right_price','right_minimum_nights','right_number_of_reviews',
'right_last_review','right_reviews_per_month','right_calculated_host_listings_count',
'right_availability_365'
])
listing2019=listing2019.rename(columns={'id':'left_id','host_id':'left_host_id','host_name':'left_host_name',
'neighbourhood_group':'left_neighbourhood_group','neighbourhood':'left_neighbourhood','latitude':'left_latitude','longitude':'left_longitude',
'room_type':'left_room_type','price':'left_price','minimum_nights':'left_minimum_nights','number_of_reviews':'left_number_of_reviews',
'last_review':'left_last_review','reviews_per_month':'left_reviews_per_month','calculated_host_listings_count':'left_calculated_host_listings_count',
'availability_365':'left_availability_365'})

listing2021=listing2021.rename(columns={'id':'right_id','host_id':'right_host_id','host_name':'right_host_name',
'neighbourhood_group':'right_neighbourhood_group','neighbourhood':'right_neighbourhood','latitude':'right_latitude','longitude':'right_longitude',
'room_type':'right_room_type','price':'right_price','minimum_nights':'right_minimum_nights','number_of_reviews':'right_number_of_reviews',
'last_review':'right_last_review','reviews_per_month':'right_reviews_per_month','calculated_host_listings_count':'right_calculated_host_listings_count',
'availability_365':'right_availability_365'})
print(listing2019.head)
print(listing2021.head)
dataset=pd.merge(listing2019, listing2021, on='name', how='outer')
dataset=dataset.rename(columns={'name':'left_name'})
dataset.insert(18,'right_name',dataset['left_name'],True)
listing2019=listing2019.rename(columns={'name':'left_name','left_host_name':'host_name'})
listing2021=listing2021.rename(columns={'name':'right_name','right_host_name':'host_name'})
datalabelzero=pd.merge(listing2019,listing2021,on ='host_name',how='outer')
datalabelzero=datalabelzero.rename(columns={'host_name':'left_host_name'})
datalabelzero.insert(20,'right_host_name',datalabelzero['left_host_name'],True)
print(datalabelzero)
pd.concat([datalabelzero, dataset, dataset]).drop_duplicates(keep=False)

print(datalabelzero)

dataset['label']=1

datalabelzero['label']=0
dataset=dataset.append(datalabelzero)
print(dataset.columns.values)

dataset.sample(frac=1)

dataset.dropna(subset=['label'])

print(dataset.head)
dataset.to_csv('Setbandb/dataset.csv')

df= pd.read_csv("Setbandb/dataset.csv")
train, validation, test = np.split(df.sample(frac=1, random_state=42),[int(.6*len(df)), int(.8*len(df))])


#train = train.iloc[: , 1:]
#test = test.iloc[: , 1:]
#validation = validation.iloc[: , 1:]
del train['Unnamed: 0']
del test['Unnamed: 0']
del validation['Unnamed: 0']

#train.rename(columns={'Unnamed: 0': 'id'}, inplace=True)
#test.rename(columns={'Unnamed: 0': 'id'}, inplace=True)
#validation.rename(columns={'Unnamed: 0': 'id'}, inplace=True)

print(train.columns.values)

train.to_csv('Setbandb/train.csv', index=False)
validation.to_csv('Setbandb/validation.csv', index=False)
test.to_csv('Setbandb/test.csv', index=False)


print(torch.cuda.is_available())
print(torch.device('cuda') )  
'''

train, validation, test = dm.data.process(path='Setbandb/',
    train='train.csv', validation='validation.csv', test='test.csv', 
    ignore_columns=('left_id', 'right_id','number_of_reviews_ltm','license'),
    left_prefix='left_',
    right_prefix='right_',
    label_attr='label',
    id_attr='id',
    cache="train_cache.pth")
'''

train.rename_axis(None, axis=1).rename_axis('id', axis=0)
test.rename_axis(None, axis=1).rename_axis('id', axis=0)
validation.rename_axis(None, axis=1).rename_axis('id', axis=0)

cuda2 = torch.device('cuda:0')  

print("ciao")
model = dm.MatchingModel(attr_summarizer='hybrid', attr_condense_factor='auto', attr_comparator=None, attr_merge='concat', classifier='2-layer-highway', hidden_size=300)
model.run_train(train, validation, best_save_path='best_model.pth',  epochs=20  , device=cuda2)
'''

model = dm.MatchingModel(attr_summarizer='hybrid', attr_condense_factor='auto', attr_comparator=None, attr_merge='concat', classifier='2-layer-highway', hidden_size=300)

model.load_state("best_model.pth")

model.run_eval(test)
