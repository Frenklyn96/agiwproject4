import pandas as pd
import numpy as np
import torch
import deepmatcher as dm
import nltk
import re
nltk.download('stopwords')
train = pd.read_csv("Setbandb/train.csv")
test = pd.read_csv("Setbandb/test.csv")
valid = pd.read_csv("Setbandb/validation.csv")
#df = df.sample(frac = 1).reset_index(drop=True)
#train_file = open("ditto/data/er_magellan/Textual/train.txt", "w", encoding="utf-8")
#test_file = open("ditto/data/er_magellan/Textual/test.txt", "w", encoding="utf-8")
#valid_file = open("ditto/data/er_magellan/Textual/valid.txt", "w", encoding="utf-8")

def txtfile(df,destination):
    a_file=open(destination,'w', encoding="utf-8")
    for x in range(len(df)):
        s1 = re.sub('[^0-9a-zA-Z]+', ' ',str(df.loc[x,'left_name']))
        s2 = re.sub('[^0-9a-zA-Z]+', ' ',str(df.loc[x,'right_name']))
        s = 'COL name VAL ' + s1 + "\tCOL name VAL " + s2 + "\t" + str(df.loc[x,'label'])
        #print(s)
        a_file.write(s+'\n')
    a_file.close()

def controllorighe(testo,df):
    count=0
    while True:
        count += 1
        s = testo.readline()
        if not s:
            break
        l=s.split()
        if ('COL name VAL ' in s and 'COL' in ' '.join(l[2:]) and (l[-1]=='0' or l[-1]=='1')):
            df.write(s)
    testo.close()
    df.close()
   

txtfile(train,"ditto/data/er_magellan/Textual/Airbnb/train_intermedio.txt")
txtfile(test,"ditto/data/er_magellan/Textual/Airbnb/test_intermedio.txt")
txtfile(valid,"ditto/data/er_magellan/Textual/Airbnb/valid_intermedio.txt")

txttrain=open("ditto/data/er_magellan/Textual/Airbnb/train_intermedio.txt","r",encoding="utf-8")
train_def=open("ditto/data/er_magellan/Textual/Airbnb/train.txt","w",encoding="utf-8")
controllorighe(txttrain, train_def)
txttest=open("ditto/data/er_magellan/Textual/Airbnb/test_intermedio.txt","r",encoding="utf-8")
test_def=open("ditto/data/er_magellan/Textual/Airbnb/test.txt","w",encoding="utf-8")
controllorighe(txttest,test_def)
txtvalid=open("ditto/data/er_magellan/Textual/Airbnb/valid_intermedio.txt","r",encoding="utf-8")
valid_def=open("ditto/data/er_magellan/Textual/Airbnb/valid.txt","w",encoding="utf-8")
controllorighe(txtvalid,valid_def)





